"""Copyright 2022 Donald Beyette and Michael Rugh

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

##########################################################
# NOTE: This file contains constants used by the game    #
#        - Player physics/Settings                       #
#        - World Physics/Setting                         #
#        - Mob Physics/Settings                          #
#        - Projctile Physics/Settings                    #
##########################################################

# Play around with these values to see what happens!

# PLAYER CONSTANTS
JUMP_SPEED         = 16
MOVEMENT_SPEED     = 5
MAX_HEALTH         = 100
DAMAGE             = 5
POWERUP_MULTIPLYER = 1.2

# PROJECTILE CONSTANTS
PROJECTILE_SCALING = 0.5
PROJECTILE_SPEED   = 5

# Mob CONSTANTS
MOB_DAMAGE     = 100
MOB_SPEED      = 10
MOB_SCALING    = 0.5
MOB_HP         = 10
MOB_JUMP_SPEED = 5

# MAP/WORLD CONSTANTS
TILE_SCALING      = 0.5
SPRITE_PIXEL_SIZE = 128
GRID_PIXEL_SIZE   = SPRITE_PIXEL_SIZE * TILE_SCALING

# MESS WITH GRAVITY! What happens?
GRAVITY = 1.2