"""Copyright 2022 Donald Beyette and Michael Rugh

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""


import arcade
import os

def create_animated_player(texture_path: str) -> arcade.AnimatedWalkingSprite:
    """Create a animated walking player
    Think of a turn base video game: After each action, you freeze
    Thus, a PNG image for each action (i.e., walking) is
    required to make the player sprite animated

    For Example: You create four PNG images
                1) Standing stilll
                2) Pickup foot with knee bent
                3) Extend foot with Knee bent
                4) Put foot down
        The order for walking would be: 1->2->3->4->1->2->3->4->1...
    """

    # !!! IMPORANT !!!
    # READ THIS
    """ List of images for the example sprite

        - alienBlue_front.png
        - alienBlue_jump.png
        - alienBlue_walk1.png
        - alienBlue walk2.png

        There are four variables to use to solve Task 1-4
            1) alien_front
            2) alien_walk_1
            3) alien_walk_2
            4) alien_jump
    """
    # BONUS: Change the png string to match your custom images
    # DO NOT CHANGE THESE VALUES TILL YOU DO ALL TASKS
    alien_front = arcade.load_texture(os.path.join(
        texture_path,
        "alienBlue_front.png"))

    alien_walk_1 = arcade.load_texture(os.path.join(
        texture_path,
        "alienBlue_walk1.png"))

    alien_walk_2 = arcade.load_texture(os.path.join(
        texture_path,
        "alienBlue_walk2.png"))

    alien_jump = arcade.load_texture(os.path.join(
        texture_path,
        "alienBlue_jump.png"))

    # NOTE: MUST READ! HINT BELOW!
    """
        # NOTE: Order is important. How do you walk in real life?
        # NOTE: What is the difference between going left or right?

        Example Python List:
            - A list in Python has elements:
                MyList = [element1, element2, ...]
            - You can access/get each element by going in order starting with 0
            - 0 is the same as being first in programming

            List[0] = element1
            List[1] = element2
    """

    # TODO: TASK 1
    """
        # Collect WALKING right animations
        # This list expects two elements
    """
    walking_right_animations = [

    ] # Expects at least two elements

    # TODO: TASK 2
    """
        # Collect WALKING left animations
        # This list expects two element
    """
    walking_left_animations = [

    ]

    # TODO: TASK 3
    """
        # Standing animations
        # This list expects one animation
    """
    standing_animation = [

    ]

    # TODO: TASK 4
    """
        # Collect JUMPING animations
        # This list expects one element
    """
    jumping_animations = [

    ]

    # DO NOT CHANGE
    if len(walking_right_animations) == 0:
        return None

    # Change this value. What happens?
    player_scaling = 0.5 # Mess around with this!!

    # DO NOT CHANGE
    player = arcade.AnimatedWalkingSprite(player_scaling)

    # TODO: TASK 5
    """
        # Assign each animation list to the correct variable
        # Each <replace_me> is expecting a animation list
        # Remove the hashtag ('#') to uncomment
    """
    #player.stand_left_textures  = <replace_me>
    #player.stand_right_textures = <replace_me>
    #player.walk_left_textures   = <replace_me>
    #player.walk_right_textures  = <replace_me>

    # TODO: TASK 6
    """
        # Where should the player start in the map?
        # Each <replace_me> is expecting a positive integer
    """
    #player.center_x = <replace_me>
    #player.center_y = <replace_me>

    # TODO: TASK
    """
        # What PNG should be loaded at the start of the game?
        # <replace_me> expects a single animation element type not a list
    """
    #player.texture = <replace_me>

    # DO NOT CHANGE
    return player

