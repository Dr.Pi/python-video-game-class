"""Copyright 2022 Donald Beyette and Michael Rugh

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""


import arcade
import random
import math
import os

from typing import Tuple

from game_constants import *
from map.levels import Levels

def create_projectile(
    x1          : float,
    y1          : float,
    x2          : float,
    y2          : float,
    texture_path: str) -> arcade.Sprite:
    """
    """

    # BONUS: Change to any PNG that is in the correct folder!
    # Folder: animation/images/projectiles
    projectile_png = "laserBlue01.png"

    # CREATE PROJECTILE OBJECT
    # NOTE: DO NOT MODIFY
    absolute_image_path = os.path.join(texture_path, projectile_png)
    projectile_sprite   = arcade.Sprite(
       absolute_image_path, PROJECTILE_SCALING)

    # TODO: TASK 1
    # Where should the projectile start?
    # Think about where the player is in the map
    # Imagine a grid overlayed ontop of the game
    # Posible values: x1, x2, y1, y2
    projectile_sprite.center_x = 100 # TODO: CHANGE THIS VALUE
    projectile_sprite.center_y = 200 # TODO: CHANGE THIS VALUE

    # TODO: TASK 2
    # Calculate the direction of the projectile
    # NOTE: The projectile needs to shoot in the direction of the mouse click
    """
        1) Draw a line from the player to the mouse click
        2) Draw a line straight down from the mouse cick to the ground
        3) Draw a line from the player to the end of line 2)
        4) The diagonal or slanted line is what we want to figure out!!!

        # start point = projectile origin location = (x,y)
        # end point   = mouse clicked location     = (x,y)
        # This is the angle (direction) the bullet will travel

        FORMULA = <Michael Replace>
    """
    y_diff = 50 # TODO: CHANGE THIS VALUE
    x_diff = 50 # TODO: CHANGE THIS VALUE
    angle  = math.atan2(y_diff, x_diff)

    # Debug info!
    #print(f"x1,y1:={x1,y1} | x2,y2={x2,y2}")
    #print(f"ANGLE in radians: {angle}")

    # DO NOT CHANGE
    # Rotate sprite image so it is not sideways
    projectile_sprite.angle = math.degrees(angle)

    # DO NOT CHANGE
    # Calculate bullet velocity
    projectile_sprite = calculate_projectile_velocity(
        angle, projectile_sprite)

    # DO NOT CHANGE
    return projectile_sprite

def calculate_projectile_velocity(
    angle            : float,
    projectile_sprite: arcade.Sprite) -> None:
    """
    """

    # BONUS
    # Calculate the velocity of the projectile
    # Velocilty takes two values: speed and direction
    """
        For example:
            Imagine you are racing in car and it hits top speed
            Once at top speed, the car stays at a constant speed (can't go faster)
            Thus, the car has constant velocilty and zero acceleration
            Acceleration is when you hit the gas peddle to increase the speed of the car

        Velocity requires speed and direction!
        Formula = direction * speed
        The direction result comes from TASK 2!

        # NOTE: IMPORTANT!!!!
        # To find direction, you must use one of these functions:
            math.cos()
            math.sin()

        Each of those functions are epxecting one argument

        For Example:
            math.cos(5)
            math.sin(2)
    """

    # TODO: What is the correct direction for the x-axis?
    x_direction = 0.1

    # TODO: WHat is the correct direction for the y-axis?
    y_direction = 0.1

    velocity_x = x_direction * PROJECTILE_SPEED # TODO: CHANGE THIS VALUE
    velocity_y = y_direction * PROJECTILE_SPEED # TODO: CHANGE THIS VALUE

    # DO NOT CHANGE
    projectile_sprite.change_x = velocity_x

    # DO NOT CHANGE
    projectile_sprite.change_y = velocity_y

    return projectile_sprite



