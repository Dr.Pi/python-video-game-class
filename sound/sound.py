"""Copyright 2022 Donald Beyette and Michael Rugh

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

from game_constants import *
import arcade

def level_sound() -> None:
    pass

def projectile_sound() -> arcade.sound:
    projectile_sound = arcade.sound.load_sound(":resources:sounds/laser2.wav")
    return projectile_sound

def damage_taken_sound() -> None:
    pass

def mob_destroyed_sound() -> None:
    pass

def coin_collected_sound() -> None:
    pass

def victory_sound() -> None:
    pass