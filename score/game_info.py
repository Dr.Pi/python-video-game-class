"""Copyright 2022 Donald Beyette and Michael Rugh

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""


import arcade
import os
import time
import math


def get_score(
    current_score: int, 
    object       : str, 
    player_sprite: arcade.Sprite) -> str:
    """Calculat score based on the object
    For Example:
        1) Every time a coin is collected, increase current_score by 1
        2) Every time a mob is destroyed, increase current_score  by 3

    Params:
        current_score (int): Total score for the player
        object        (str): can be coins, keys, goal, mob, powerup
        distance      (int): How far the player has traveled from the start

    Returns:
        score (int): Total score for the player
    """

    # TODO: TASK 1
    """
        # Determine how to increase the players score
        # Example below: For each coin, increase the score by 1
        # How many points should coins give?
    """
    if object == "coin":
        current_score += 1

    # TODO: TASK 2
    """
        Step 1: Crate a IF condition that matches to "mob_destroyed"
        Step 2: Change the score
    """

    # TODO: TASK 3
    """
        Step 1: Create a IF condition that matches to "death_trap"
        Step 2: Change the score
    """

    # TODO: TASK 4
    """
        Step 1: Create a IF condition that matches to "key_found"
        Step 2: Change the score
    """

    # BONUS
    """
        # Should we increase the score based on distance?
        # Only do this if you have successfully changed the score
        # With each object type: (coin, mob, key, goal, powerups)

        # TODO BONUS: Use the variable current_distance to change the score
        # DO NOT modify the line below this comment
        # NOTE: player_sprite.center_x and player_sprite.center_y
    """
    #current_distance = get_distance()

    # BONUS: Change score based on distance

    # DO NOT CHANGE
    return current_score

def get_distance(
    start_x: int, 
    start_y: int,
    current_x: int,
    current_y: int) -> int:
    """ Calculate the disance between two points.
    
        Params:
            start_x (int): player starting x location 
            start_y (int): player starting y location

            current_x (int): Current x location for the player
            current_y (int): Current y location for the player

        Returns:
            distance (int): math.sqrt((x2-x1)**2 + (y2-y1)**2)
    """
    distance = 0

    # TODO BONUS: Calculate the distance between:
    # (start_x, start_y) and (current_x, current_y)


    # DO NOT CHANGE
    return distance
    
