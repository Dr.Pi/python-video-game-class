import arcade
import math

from typing import Tuple


MOVING_PLATFORM_1_POSITIONS = [
    (2720, 160), # TODO: TASK 1: CHANGE MY VALUE
    (2720, 160)
]

MOVING_PLATFORM_2_POSITIONS = [
    (4748, 208), # TODO: TASK 1: CHANGE MY VALUE!
    (4748, 208)
]

def first_moving_tile(
    moving_tile   : arcade.Sprite,
    position_index: int) -> int:
    """Make two tiles move back and forth

        Params:
            moving_tile        (arcade.Sprite): First moving platform by lava
W            position_index              (int): 0 if moving to the right
                                                1 if moving to the left

        QUESTION: How can we make the floating platforms move correctly?

        NOTE: Pick two points and draw a line.
        We need to figure out the displacement and change direction when
        the displacement is zero!
    """
    # BONUS: Determine what a good moving_speed is
    moving_speed = 5

    # TODO: Task 1
    """Calculate the change in x & y between two points
    Update the second element in each of the following lists below:
        1) MOVING_PLATFORM_1_POSITIONS
        2) MOVING_PLATFORM_2_POSITIONS
        3) First point : Starting location for the first moving platform
        4) Second point: Current location for the firs tmoving platform

        NOTE:
            x_diff = x2-x1
            y_diff = y2-y1


        NOTE: Descriptions for each moving platform:
            1) Platform over the lava
            2) Playform over the boss at the end

        We need both of them to move between two points.
        What should the second point be for each platform?

        NOTE: Draw on paper what the map looks like with both platforms
        Draw a line from the start of the platform to how far you want it to travel.
        How can we determine when it should turn around? (Move back and forth)
    """

    x_diff = moving_tile.center_x - MOVING_PLATFORM_1_POSITIONS[position_index][0]
    y_diff = moving_tile.center_y - MOVING_PLATFORM_1_POSITIONS[position_index][1]

    # TODO: TASK 2
    """Calculate the distance between two points first moving platform
    """
    current_distance = math.sqrt(x_diff**2+y_diff**2) #TODO: Change my values

    # TODO: TASK 3
    """
        Step 1): Compare the position_index to zero and update change_x with a
                 position move_speed value

        Step 2): Compare the position_index to one and update change_x with a
                 negative move_speed value

        # NOTE: As the tile moves closer to the ending points on your line,
                the distance decreases and will adventually be zero or less
                When this happens, we want it to turn around and head back to
                our first location
    """
    if position_index == 0:
        moving_tile.change_x = -moving_speed # TODO: Change my value
    else:
        moving_tile.change_x = moving_speed # TODO: Change my value

    # DO NOT EDIT
    if current_distance <= moving_speed or current_distance > 500:
        if position_index == len(MOVING_PLATFORM_1_POSITIONS)-1:
            position_index = 0
        else:
            position_index = 1

    return position_index

def second_moving_tile(
    moving_tile    : arcade.Sprite,
    position_index : int) -> int:
    """Make two tiles move back and forth

        Params:
            moving_tile  (arcade.Sprite): First moving platform by the boss
            position_index         (int): 0 if moving to the right
                                          1 if moving to the left

        QUESTION: How can we make the floating platforms move correctly?

        NOTE: Pick two points and draw a line.
        We need to figure out the displacement and change direction when
        the displacement is zero!
    """
       # BONUS: Determine what a good moving_speed is
    moving_speed = 5

    # TODO: Task 4
    """Calculate the change in x & y between two points
        1) First point : Starting location for the first moving platform
        2) Second point: Current location for the firs tmoving platform

        NOTE:
            x_diff = x2-x1
            y_diff = y2-y1

    """
    x_diff = moving_tile.center_x  - MOVING_PLATFORM_2_POSITIONS[position_index][0]
    y_diff = moving_tile.center_y - MOVING_PLATFORM_2_POSITIONS[position_index][1]

    # TODO: TASK 5
    """Calculate the distance between two points first moving platform
    """
    current_distance = math.sqrt(x_diff**2+y_diff**2) #TODO: Change my values

    # TODO: TASK 6
    """
        Step 1): Compare the position_index to zero and update change_x with a
                 position move_speed value

        Step 2): Compare the position_index to one and update change_x with a
                 negative move_speed value

        # NOTE: As the tile moves closer to the ending points on your line,
                the distance decreases and will adventually be zero or less
                When this happens, we want it to turn around and head back to
                our first location
    """
    if position_index == 0:
        moving_tile.change_x = -moving_speed # TODO: Change my value
    else:
        moving_tile.change_x = moving_speed # TODO: Change my value

    # DO NOT EDIT
    if current_distance <= moving_speed or current_distance > 500:
        if position_index == len(MOVING_PLATFORM_2_POSITIONS)-1:
            position_index = 0
        else:
            position_index = 1

    return position_index

